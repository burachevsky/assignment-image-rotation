#include "image.h"

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTIL_H

struct image rotate( struct image const source );

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTIL_H
