#include <stdlib.h>
#include "image.h"

void image_destroy( struct image img ) {
    free( img.data );
}

