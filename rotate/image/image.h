#include <stdint.h>

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_destroy( struct image img );

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H


