#include "image_util.h"

#include <stdlib.h>

struct image rotate( struct image const source ) {
    struct image img = {
        .width = source.height,
        .height = source.width,
        .data = malloc( source.width * source.height * sizeof( struct pixel ) )
    };

    for (uint64_t i = 0; i < source.height; ++i) {
        for (uint64_t j = 0; j < source.width; ++j) {
            size_t old_pos = i * source.width + j;
            size_t new_pos = (img.height - j - 1) * img.width + i ;
            img.data[new_pos] = source.data[old_pos];
        }
    }

    return img;
}