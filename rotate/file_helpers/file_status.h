#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_STATUS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_STATUS_H

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

char* read_status_to_string( enum read_status status );

char* write_status_to_string( enum write_status status );

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_STATUS_H
