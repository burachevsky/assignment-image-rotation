#include "file_status.h"

static char* read_status_strings[] = {
        [READ_OK] = "file successfully read",
        [READ_INVALID_BITS] = "invalid pixel data",
        [READ_INVALID_HEADER] = "invalid header"
};

static char* write_status_strings[] = {
        [WRITE_OK] = "file was written successfully",
        [WRITE_ERROR] = "can't write file"
};


char* read_status_to_string( enum read_status status ) {
    return read_status_strings[status];
}

char* write_status_to_string( enum write_status status ) {
    return write_status_strings[status];
}
