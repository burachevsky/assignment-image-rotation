#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define BFTYPE 0x4d42
#define BISIZE 40
#define BIBITCOUNT 24
#define BIPLANES 1
#define DEFAULT 0

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static uint32_t compute_padding( uint32_t width ) {
    uint32_t mod = (width * sizeof( struct pixel )) % 4;
    return mod == 0 ? mod : 4 - mod;
}

static struct bmp_header bmp_header_create( struct image const* img ) {
    struct bmp_header header = { 0 };

    header.bfType = BFTYPE;
    header.bfileSize = sizeof( struct bmp_header ) +
            ( sizeof( struct pixel ) * img->width + compute_padding(img->width)) * img->height;
    header.bfReserved = DEFAULT;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BISIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BIPLANES;
    header.biBitCount = BIBITCOUNT;
    header.biCompression = DEFAULT;
    header.biSizeImage = header.bfileSize - sizeof( struct bmp_header );
    header.biXPelsPerMeter = DEFAULT;
    header.biYPelsPerMeter = DEFAULT;
    header.biClrUsed = DEFAULT;
    header.biClrImportant = DEFAULT;

    return header;
}

static bool bmp_header_validate( struct bmp_header* header ) {
    return  header->bfType == BFTYPE ||
            header->biCompression == 0;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = { 0 };
    if (!read_header( in, &header ) || !bmp_header_validate( &header ))
        return READ_INVALID_HEADER;

    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;

    struct pixel* pixels = malloc( width * height * sizeof( struct pixel ) );

    const size_t line_len = width * sizeof( struct pixel );
    const size_t padding = compute_padding(width);
    char padding_bytes[3] = {0};

    for (uint32_t i = 0; i < height; ++i) {
        if (fread( pixels + i * width, line_len, 1, in ) != 1) {
            free( pixels );
            return READ_INVALID_BITS;
        }

        if (padding > 0) {
            if (fread( &padding_bytes, padding, 1, in ) != 1) {
                free( pixels );
                return READ_INVALID_BITS;
            }
        }
    }

    img->width = width;
    img->height = height;
    img->data = pixels;

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    const struct bmp_header header = bmp_header_create( img );

    if (fwrite( &header, sizeof( struct bmp_header ), 1, out ) != 1)
        return WRITE_ERROR;

    const uint32_t width = img->width;
    const uint32_t height = img->height;
    const size_t line_len = width * sizeof( struct pixel );
    const size_t padding = compute_padding(width);
    const char padding_bytes[3] = {0};

    for (uint32_t i = 0; i < height; ++i) {
        if (fwrite( img->data + i * width, line_len, 1, out ) != 1)
            return WRITE_ERROR;

        if (padding > 0)
            if (fwrite( &padding_bytes, padding, 1, out ) != 1)
                return  WRITE_ERROR;
    }

    return WRITE_OK;
}
