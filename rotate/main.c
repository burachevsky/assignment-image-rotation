#include "format/bmp.h"
#include "util.h"
#include "image/image_util.h"

#include <stdio.h>

void usage() {
    fprintf(stderr, "Usage: ./rotate BMP_FILE_NAME\n");
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) error("Not enough arguments \n" );
    if (argc > 2) error("Too many arguments \n" );

    logln("Opening file...\n");
    FILE* file = fopen( argv[1], "rb+" );
    if (!file)
        error( "Failed to open file" );

    logln("Reading BMP...\n");
    struct image img = { 0 };
    enum read_status rs = from_bmp( file, &img );
    if (rs != READ_OK)
        error( "Invalid BMP file: %s", read_status_to_string( rs ) );

    logln("Rotating image...\n");
    struct image new_img = rotate( img );

    logln("Writing BMP file...\n");
    fseek( file, 0, SEEK_SET );
    enum write_status ws = to_bmp( file, &new_img );
    if (ws != WRITE_OK)
        error( write_status_to_string( ws ) );

    image_destroy( new_img );
    image_destroy( img );
    fclose( file );

    fprintf(stdout, "%s \n", "Image was successfully rotated");

    return 0;
}
